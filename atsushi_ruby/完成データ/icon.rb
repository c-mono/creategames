class Icon

  SIZE = 8
  EMPTY = 0
  DOT = 1

  attr_accessor :board

  def init_board
    @board = Array.new(SIZE+3){Array.new(SIZE+5,EMPTY)}
  end

  def show_icon
    @board.each_with_index do |col|
      col.each do |row|
        case row
          when EMPTY
            print("　")
          when DOT
            print("■")
        end
      end
      print("\n")
    end
    print("\n")
  end

  def show_default
    init_board
    print "\e[31m"
    @board[3][2] = DOT
    @board[2][3] = DOT
    @board[3][4] = DOT
    @board[3][7] = DOT
    @board[2][8] = DOT
    @board[3][9] = DOT
    @board[6][2] = DOT
    @board[7][3] = DOT
    @board[8][4] = DOT
    @board[8][5] = DOT
    @board[8][6] = DOT
    @board[8][7] = DOT
    @board[7][8] = DOT
    @board[6][9] = DOT
    print "\e[0m"
    show_icon 
    init_board
  end

  def show_denger
    init_board
    @board[2][2] = DOT
    @board[2][3] = DOT
    @board[2][4] = DOT
    @board[2][7] = DOT
    @board[2][8] = DOT
    @board[2][9] = DOT
    @board[5][4] = DOT
    @board[5][5] = DOT
    @board[5][6] = DOT
    @board[5][7] = DOT
    @board[6][3] = DOT
    @board[6][8] = DOT
    @board[7][3] = DOT
    @board[7][8] = DOT
    @board[7][4] = DOT
    @board[7][5] = DOT
    @board[7][6] = DOT
    @board[7][7] = DOT
    show_icon
    init_board
  end

  def show_dying
    init_board
    @board[2][4] = DOT
    @board[3][3] = DOT
    @board[4][2] = DOT
    @board[2][8] = DOT
    @board[3][9] = DOT
    @board[4][10] = DOT
    @board[7][4] = DOT
    @board[7][5] = DOT
    @board[7][6] = DOT
    @board[7][7] = DOT
    @board[7][8] = DOT
    @board[8][3] = DOT
    @board[8][9] = DOT
    show_icon
    init_board
  end
end

#icon = Icon.new
#icon.show_default
#icon.show_denger
#icon.show_dying
