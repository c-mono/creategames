require 'rdbi'
require 'rdbi-driver-sqlite3'

class Chara
  def initialize(sqlite_name)
    @db_name = sqlite_name
    @dbh = RDBI.connect(:SQLite3, :database => @db_name )
  end

attr_accessor :user_chara, :enemy_chara, :u_name, :u_hp, :u_mp, :u_atk, :u_def, :u_job, :r_name, :e_hp, :e_mp, :e_atk, :e_def, :e_job

  def list_data
    item_name = {:id => '', :name => "名前",
                 :hp => "HP", :mp => "MP",
                 :atk => "攻撃力", :def => "防御力",
                 :agi => "回避力", :job => "役職" }

    sth = @dbh.execute("select name, hp, mp, atk, def, job from characters")
    columns = sth.schema.columns.to_a

    sth.each do |row|
      row.each_with_index do |val, index|
=begin
        if :name == columns[index].name
          puts "#{item_name[columns[index].name]} :#{val.to_s}"
        else
          print "#{item_name[columns[index].name]} :#{val.to_s}"
        end
=end
        print "#{item_name[columns[index].name]}:#{val.to_s}  "
        print "\n" if :name == columns[index].name
      end
    puts "\n-----------------"
    end
  end

  def select_comment
    puts "
 ___________________________________
|                                   |
|  キャラクターを選択してください。 |
|___________________________________|"

    puts "
     [1]           [2]           [3]            [4]            [5]
                   ___                           ___
     ,,,         _|___|_         ......       _／___＼_       qqqqq
 ヽ(・_・)ﾉ      J-L-|||      ⊂(`ω´∩)     (＊‥＊)o      ( ﾟ､ゝﾟ)
    |   |         |∞ |>         |  +   /     ノ  :  Y       /b   d
_____U_U___________U_U_____________U___U_________U_U___________U_U___________

"

  end

  def select_chara
    sth = @dbh.execute("select id, name, hp, mp, atk, def, job from characters")
    chara = sth.to_a
    while true
      puts "[1]たろう [2]エリック [3]ハルウララ [4]ジェシカ [5]ティーダ"
      print ">"
      num = gets.chomp
      case num
      when '1'
        @user_chara = chara[0]
        break
      when '2'
        @user_chara = chara[1]
        break
      when '3'
        @user_chara = chara[2]
        break
      when '4'
        @user_chara = chara[3]
        break
      when '5'
        @user_chara = chara[4]
        break
      else
       puts "\n存在しない値です"
      end
    end
    puts "--------------------"
    puts "操作キャラデータ"
    puts @user_chara
  end

  def enemy_select
    sth = @dbh.execute("select id, name, hp, mp, atk, def, job from characters")
    selected_enemy = sth.to_a
    @enemy_chara =  selected_enemy.sample
    puts "--------------------"
    puts "敵キャラデータ"
    puts @enemy_chara
  end

  def chara_data_store
    @u_name = @user_chara[1]
    @u_hp = @user_chara[2]
    @u_mp = @user_chara[3]
    @u_atk = @user_chara[4]
    @u_def = @user_chara[5]
    @u_job = @user_chara[6]

    @e_name = @enemy_chara[1]
    @e_hp = @enemy_chara[2]
    @e_mp = @enemy_chara[3]
    @e_atk = @enemy_chara[4]
    @e_def = @enemy_chara[5]
    @e_job = @enemy_chara[6]
#   @dbh.disconnect 
  end


end

#ino = Chara.new("chara_data.db")

#ino.list_data

#ino.select_chara

#ino.enemy_select

#ino.chara_data_store
