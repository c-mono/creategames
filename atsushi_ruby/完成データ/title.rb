class Title

  def game_start
    while true
      puts "
      ______________________
 ____|                      |____
 ＼  |      GAME TITLE      |   ／
 ／  l______________________|   ＼
 ──────                     ──────

           [0]はじめる
           [1]ルール説明
"
print "
            >"
      key = gets.chomp
      case
        when key == '0'
          game_loading
          break
        when key == '1'
          game_rule
          break
      else
        break
      end
    end
  end

  def game_rule
    while true
    puts"
  ___________________________________________________________
 |                                                           |＼
 |   ～ルール説明～                                          |  ＼
 |　　・コマンド                                             |    ＼
 |　　「たたかう」：相手に攻撃します。                       |______＼
 |　　「ガード」：攻撃から身を守ります。                              |
 |　　「にげる」：戦闘から離脱します。                                |
 |      ※CPキャラのコマンドはランダムで取得します。                  |
 |                                                                    |
 |  ・ダメージ                                                        |
 |　　キャラクターの攻撃力と、相手の防御力の引算                      |
 |     ＋0～5(ランダム)がダメージとなります。                         |
 |                                                                    |
 |  ・画面表示                                                        |
 |    顔アイコン：キャラの体力によって変化します。                    |
 |    HP,MP：1ブロック=10として表示しています。                       |
 |　 　例：HP[====]はHP=40という意味です。                            |
 |                                                                    |
 |  ・勝敗                                                            |
 |　　自分のキャラもしくはCPキャラの体力を0にした方が勝ちとなります。 |
 |　                                                                  |
 |____________________________________________________________________|

  [0]タイトル画面に戻る
"
    print ">"
    key = gets.chomp
      case
        when key == '0'
          game_start
          break
      end
    end
  end

  def game_loading
    print "ゲームを読み込んでいます... \n["
    sleep 0.2
    print "■"
    sleep 2
    2.times {
      print "■"
      sleep 0.1 }
    print "■"
    sleep 0.1
    2.times {
     print "■"
     sleep 1}
    print "■"
    sleep 0.1
    print "■"
    sleep 0.8
    print "]"
    sleep 0.5
    print "完了！\n"
    sleep 1
  end

end
