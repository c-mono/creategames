require './chara_data'
require 'rdbi'
require 'rdbi-driver-sqlite3'

class Guard

  attr_accessor :u_mp_gauge, :e_mp_gauge

  def magic_point(chara)
#    @user_name = chara.u_name
    @u_mp = chara.u_mp.to_i
#    @enemy_name = chara.e_name
    @e_mp = chara.e_mp.to_i
  end

  def init_u_mp #ユーザーキャラのMPの定義
    u_mp = @u_mp /10
    @u_mp_gauge = [].fill("＃",0,u_mp)
  end

  def u_exe_guard #ユーザーがガードを実行した時の処理
    @u_mp_gauge.shift(1)
  end

  def print_u_mp #ユーザーのMPを表示する定義
    print "ユーザーＭＰ:["
    @u_mp_gauge.each do |mp|
      print mp
    end
      print "]\n"
  end

  def init_e_mp #敵キャラのMPの定義
    puts @enemy_name
    e_mp = @e_mp /10
    @e_mp_gauge = [].fill("＃",0,e_mp)
  end

  def e_exe_guard #敵がガードを実行した時の処理
    @e_mp_gauge.shift(1)
  end

  def print_e_mp #敵のMPを表示する定義
      print "敵ＭＰ:["
      @e_mp_gauge.each do |mp|
        print mp
      end
      print "]\n"
  end


  def damage_test #ダメージ処理ができてるか確認用メソッド
    print_u_mp
    print_e_mp
      print "
ユーザー ガード使用 u
エネミー ガード使用 e:"
    while true
      eu = gets.chomp
      case eu
      when 'e'
        e_exe_guard
        print_u_mp
        print_e_mp
      when 'u'
        u_exe_guard
        print_u_mp
        print_e_mp
      end
    end
  end


end

if __FILE__ == $0
  chara = Chara.new('chara_data.db')
  chara.select_chara
  chara.enemy_select
  chara.chara_data_store
  guard = Guard.new
  guard.magic_point(chara)
  guard.init_u_mp
  guard.init_e_mp
#  guard.print_u_mp
#  guard.u_exe_guard
#  guard.print_e_mp
  guard.damage_test
end
