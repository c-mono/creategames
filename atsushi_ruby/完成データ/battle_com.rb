require './attack'
require './icon'
require './guard_system'
require './chara_data'
#require './game_finish.rb'
require 'rdbi'
require 'rdbi-driver-sqlite3'

class CommandProcess
  def initialize
    @icon = Icon.new
    @chara = Chara.new('chara_data.db')
    @guard = Guard.new
    @attack = Attack.new(@guard)
    @exit = Exit.new
  end

#  def mp_data
#    @guard.magic_point(chara)
#    @guard.init_u_mp
#    @guard.init_e_mp
#  end

  def enemy_comes
        puts "
 __________________________________________
|                                          |                                
|      てき　は　まだ　たおれない！▼      |
|                                          |
|   どうする？                             |
|      [0]：たたかう                       |
|      [1]：ガードする                     |
|      [2]：にげる                         |
|__________________________________________|"
                print">"
  end

  def user_command
            puts "
 __________________________________________
|                                          |                                
|     どうする？                           |
|        [0]：たたかう                     |
|        [1]：ガードする                   |
|        [2]：にげる                       |
|__________________________________________|"
                print">"
        print">"
  end

#コマンド実行
  def com_run
    while true
      @chara.select_comment
      @chara.list_data
      @chara.select_chara
      @chara.enemy_select
      @chara.chara_data_store
      @guard.magic_point(@chara)
      @guard.init_u_mp
      @guard.init_e_mp
    #コマンド表示　敵のところはデータベースから持ってくる
      sleep 1
      @attack.all_status(@chara)
      @attack.init_e_hp(@chara)
      @guard.print_e_mp
      puts "　"
      @attack.init_u_hp(@chara)
      @guard.print_u_mp
      sleep 1
      puts "
   __________________________________________
  |                                          |
  |       てき　が　おそってきた！▼         |
  |                                          |
  |   どうする？                             |
  |      [0]：たたかう                       |
  |      [1]：ガードする                     |
  |      [2]：にげる                         |
  |__________________________________________|"
      print">"

      while true
        #ユーザー入力待ち
        command = gets.chomp
        case
          #コマンド処理
        when command == "0"
          @icon.show_default
          puts"　　てきに　こうげきを　しかけた！▼"
          #たたかうコマンドmethod
          is_restart = @attack.user_do(@chara)
          if true == is_restart
            break
          elsif false == is_restart 
            return
          end
          @guard.print_e_mp
          is_restart = @attack.e_choice(@chara)
          if true == is_restart
            break
          elsif false == is_restart 
            return
          end
          @guard.print_u_mp
          enemy_comes

        when command == "1"

          #ガードコマンドmethod
          @icon.show_default
          @attack.change_e_hp(@chara)          
          @guard.print_e_mp
          @attack.change_u_hp(@chara)
          @guard.print_u_mp
          if @guard.u_mp_gauge.size == 0  #MPゲージが0か1以上かの条件分岐
            puts "\n　　もう　ガードできない!▼"
            sleep 1
          else 
            puts "　　てきの　こうげきをふせいだ！▼"
            @guard.u_exe_guard #MP攻撃を防いだ時だけMP消費
            sleep 1
          end 
          enemy_comes

        when command == "2"
          #メッセージ表示
  #    attack.user_do(chara)
  #        @guard.print_u_mp
  #    attack.enemy_do(chara)
  #        @guard.print_e_mp
  #        enemy_comes
          sleep 0.5
          puts"\n
  ＼　逃げられると思った？　／"
          @icon.show_default
          print "
   _________________________________________________
  |                                                 |
  |  みえない　かべに　はばまれて　にげられない！▼ |
  |_________________________________________________|
  "
          @attack.change_e_hp(@chara)
          @guard.print_e_mp
          @attack.change_u_hp(@chara)
          @guard.print_u_mp
          enemy_comes

          #コマンド選択画面に戻る

          # when 体力 == 0
          #終了画面処理

        else
          #コマンド選択画面に戻る
        end
      end
    end
  end
end
#コマンド実行用インスタンス作成
#コマンド実行
if __FILE__ == $0
#  chara = Chara.new('chara_data.db')
#  chara.select_chara
#  chara.enemy_select
#  chara.chara_data_store
#attack = Attack.new
#attack.all_status(chara)
#  guard = Guard.new
#  guard.magic_point(chara)
#  guard.init_u_mp
#  guard.u_exe_guard
#  guard.print_u_mp
#  guard.init_e_mp
#  guard.e_exe_guard
#  guard.print_e_mp
end

#commandprocess = CommandProcess.new
#commandprocess.com_run#(attack,chara)
