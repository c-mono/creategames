require './chara_data.rb'

class Exit

  def initialize
    @chara_select = Chara.new('chara_data.db')   
  end


  def game_finish_choice
    puts "
もう一回挑戦しますか？
[0]はい
[1]いいえ(ゲーム終了)
"
    print ">"
    key = gets.chomp
    case
      when key == '0'
        @chara_select
      when key == '1'
        print "
＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
|                                                      |
|               Thank You For Playing!                 |
|＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿|\n"
        puts "\n"
        exit!
    end
  end

  def win
#        puts "戦闘に勝利した！"
    print "
 ＿＿＿＿＿＿＿＿＿＿
|                    |                                                 
|                    |
|  戦闘に勝利した!   |
|                    |
|＿＿＿＿＿＿＿＿＿＿|"
    game_finish_choice
  end
  
  def lose
#       puts "目の前が真っ暗になった..."
    print "
 ＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿
|                              |
|                              |
|  目の前が真っ暗になった...   |
|                              |
|＿＿＿＿＿＿＿＿＿＿＿＿＿＿＿|" 
    game_finish_choice
  end

end
