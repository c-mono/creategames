require './chara_data'
require './guard_system'
require './game_finish.rb'
require './icon.rb'
require 'rdbi'
require 'rdbi-driver-sqlite3'

#攻撃コマンド処理

#attr_accessor :u_hp_gauge, :e_hp_gauge


class Attack

  def initialize(guard)
    @exit = Exit.new
    @icon = Icon.new
    @guard = guard
#    @compro = CommandProcess.new
    
  end

  def all_status(chara)
    @u_hp = chara.u_hp.to_i
    @u_atk = chara.u_atk.to_i 
    @u_def = chara.u_def.to_i
    @e_hp = chara.e_hp.to_i
    @e_atk = chara.e_atk.to_i
    @e_def = chara.e_def.to_i
  end

  def e_choice(chara)
     enemy_choice = [0,1]
     r_enemy_choice =  enemy_choice.sample
#     puts r_enemy_choice
       if r_enemy_choice == 0
         return enemy_do(chara)
         puts "　　てきの　こうげきで　HPが#{@e_num1}へった！"
       elsif r_enemy_choice == 1
          @guard.e_exe_guard
         puts "　　てきに　ガード　された！"
         @e_hp_num += @u_num1
       elsif @guard.e_mp_gauge.size == 0  #MPゲージが0か1以上かの条件分岐
         puts "　　てきは　ガードが　つかえないようだ!"
#       else 
#         puts "てきの　こうげきをふせいだ!"
#         @guard.e_exe_guard #MP攻撃を防いだ時だけMP消費
       end
  end

  def init_u_hp(chara)
   u_hp = @u_hp / 10
   @u_hp_gauge = [].fill("＝",0,u_hp)
   print"ユーザーＨＰ:["
      @u_hp_gauge.each do |hp|
        print hp
      end
   print "]\n"
   @u_hp_num = @u_hp
  end

  def init_e_hp(chara)
   @icon.show_default
   e_hp = @e_hp / 10
   @e_hp_gauge = [].fill("＝",0,e_hp)
   print"敵ＨＰ:["
      @e_hp_gauge.each do |hp|
        print hp
      end
   print "]\n"
   @e_hp_num = @e_hp
  end
#アイコン表示の処理
  def user_do(chara)
    u_r = rand(5)
    num = @u_atk - @e_def 
    @u_num1 = u_r + num
    @e_hp_num -= @u_num1
    return change_e_hp(chara)
  end

  def enemy_do(chara)
    e_r = rand(5)
    num = @e_atk - @u_def
    @e_num1 = e_r + num
    @u_hp_num -= @e_num1
    return change_u_hp(chara)
  end

  def change_u_hp(chara)
    u_hp_num = @u_hp_num / 10
    @change_u_hp_gauge = [].fill("＝",0,u_hp_num)
    print"
自分
HP:["
      @change_u_hp_gauge.each do |hp|
        print hp
      end
    print "]\n"
    if @u_hp_num <= 0
      return @exit.lose
    end
  end

  def change_e_hp(chara)
    @icon.show_default
    e_hp_num = @e_hp_num / 10
    @change_e_hp_gauge = [].fill("＝",0,e_hp_num)
    print"
敵
HP:["
      @change_e_hp_gauge.each do |hp|
        print hp
      end
    print "]\n"
    if @e_hp_num == 3 
      @icon.show_denger
      puts "てきは　しにかけている！"
    elsif @e_hp_num <= 0
      puts "
＼ま、まけた..／"
      @icon.show_dying
      return @exit.win
#      @compro.com_run
    end
  end

end



#   chara = Chara.new('chara_data.db')
if __FILE__ == $0
   chara.select_chara
   chara.enemy_select
   chara.chara_data_store
  attack = Attack.new
  attack.all_status(chara)
  attack.init_e_hp(chara)
  attack.init_u_hp(chara)
  attack.e_choice(chara)
  attack.enemy_do(chara)
  attack.e_choice(chara)
  attack.enemy_do(chara)
  attack.e_choice(chara)
  attack.enemy_do(chara)
end


